# ApolloOpener

Open Reddit links in the Apollo reddit app using [libOpener](https://github.com/hbang/libopener).

The following type of Reddit links are supported:
- https://www.reddit.com/
- https://old.reddit.com/
- https://m.reddit.com/
- https://reddit.com/
- https://redd.it/

Tested and working on Apollo v1.2.3 and libopener v3.2.2 on iOS 11.3.1

Hosted on my personal repo: https://vantablack.gitlab.io/cydia-repo/

This tweak is a forked version of https://github.com/gilshahar7/ApolloOpener but updated to fix an issue with `old.reddit` and `m.reddit` links.

## Develop

To make the package:

``` bash
$ make package
```

To make the package and install:

``` bash
$ iproxy 2222 22
$ export THEOS_DEVICE_IP=localhost
$ export THEOS_DEVICE_PORT=2222
```